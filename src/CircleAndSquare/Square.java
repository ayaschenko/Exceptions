package CircleAndSquare;

public class Square extends CalculateSP {
    float sideSize;

    @Override
    public void Perimetr(float sideSizeOrRadius) {

        this.sideSize = sideSizeOrRadius;
        float perimetrOfsquare = 4*sideSizeOrRadius;
        float squareOfsquare = sideSizeOrRadius*sideSizeOrRadius;
        System.out.println("Square perimeter - "+perimetrOfsquare + " "+ "Square of square - "+squareOfsquare);

    }

    }
