package ExeptionsLesson;

import java.util.Scanner;

public class IllegalArg {
    public static void main(String[] args) {
        IllegalArg illegalArg = new IllegalArg();
        Scanner in = new Scanner(System.in);
        System.out.println("Enter number: ");
        int number = in.nextInt();

        try {
            illegalArg.run(number);


        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    public void run(int i) {
        if (i < 10 || i > 8) {
        }
    }

    }

